// Fonction pour afficher le menu (navigation téléphone), donné par Bulma.
(function() {
    var burger = document.querySelector('.burger');
    var menu = document.querySelector('#'+burger.dataset.target);
    burger.addEventListener('click', function() {
        burger.classList.toggle('is-active');
        menu.classList.toggle('is-active');
    });
})();

// Slider
var slider = {
    timer: 0,
    tickrate: 10000, // Le nombre de milliseconde entre chaques images
    index: 0, // L'index de mon image dans la fonction slider
    src: {
        "0": "./assets/img/slider_0.jpg",
        "1": "./assets/img/slider_1.jpg",
        "2": "./assets/img/slider_2.jpg",
        "3": "./assets/img/slider_3.jpg",
        "4": "./assets/img/slider_4.jpg",
        "5": "./assets/img/slider_5.jpg"
    },
    caption: {
        "0": "Les usines",
        "1": "L'habitat de l'ours polaire en danger",
        "2": "La fonte des glaciers",
        "3": "La fonte des glaciers",
        "4": "La polution humaine",
        "5": "La polution humaine"
    }
}

function ft_slider() {   
    let timer = setTimeout(() => {
        if (slider.index >= 5) // Si l'index est supérieur ou égale à 5
            slider.index = 0
    
        $('#pl_slider').fadeOut(1000);
        slider.index += 1;

        setTimeout(() => {
            $('#pl_slider_img').attr("src", slider.src[slider.index]);
            $('#pl_slider_cp').text(slider.caption[slider.index]);
            $('#pl_slider').data('img', slider.index);
            $('#pl_slider').fadeIn(1000);
        }, 1000);

        ft_slider();
    }, slider.tickrate);
    slider.timer = timer;
}

function ft_slider_move(direction) {
    switch (direction) {
        case 'left':
            clearTimeout(slider.timer); // Clear du timeout
            if (slider.index - 1 < 0) // Si l'index - 1 est inférieur à 0
                slider.index = 5;
            else
                slider.index -= 1;
            $('#pl_slider').fadeOut(500);
            setTimeout(() => {
                $('#pl_slider_img').attr("src", slider.src[slider.index]);
                $('#pl_slider_cp').text(slider.caption[slider.index]);
                $('#pl_slider').data('img', slider.index);
                $('#pl_slider').fadeIn(500);
            }, 500);
            break;
        case 'right':
            clearTimeout(slider.timer); // Clear du timeout
            if (slider.index >= 5) // Si l'index est supérieur ou égale à 5
                slider.index = 0
            else
                slider.index += 1;
            $('#pl_slider').fadeOut(500);
            setTimeout(() => {
                $('#pl_slider_img').attr("src", slider.src[slider.index]);
                $('#pl_slider_cp').text(slider.caption[slider.index]);
                $('#pl_slider').data('img', slider.index);
                $('#pl_slider').fadeIn(500);
            }, 500);
            break;
        default:
            break;
    }
    ft_slider(); // J'appelle de nouveau la fonction pour relancer le slider
}

// Report
function ft_report() {
    let report = "";
    swal("Merci de votre retour:", {
        content: "input",
        button: "Envoyer"
    })
    .then((value) => {
        value = value.trim();
        if (value.length === 0 || value.length <= 3) { // Champ non complété
            swal("Merci de compléter le champ", {
                icon: "warning",
                button: "Fermer"
            })
            .then( function () {
                ft_report();
            });
        }
        else {
            /*$.ajax({ // Requete ajax fonctionne pas en local
                type: "POST",
                url: "email.php", // Script php non fait
                data: value,
                success: function(){
                    swal("Envoi efféctué avec succes", {
                        icon: "success",
                        button: "Fermer"
                    });
                }
            });*/
            swal("Envoi efféctué avec succes", {
                icon: "success",
                button: "Fermer"
            });
        }
    });
}

$(function(){
    ft_slider(); // On démarre notre script avec la fonction slider
});